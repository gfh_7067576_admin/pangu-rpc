package com.emoke.core.pangu.config;

import com.emoke.core.pangu.annotation.ApiScan;
import com.emoke.core.pangu.annotation.ZoneMapping;
import com.emoke.core.pangu.util.ReflectUtil;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.config.ConstructorArgumentValues;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.core.type.AnnotationMetadata;
import java.util.List;
import java.util.Map;
@Slf4j
public class BeanSelector implements ImportBeanDefinitionRegistrar {

    @SneakyThrows
    @Override
    public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata, @NotNull BeanDefinitionRegistry registry) {
        AnnotationAttributes attributes = AnnotationAttributes
                .fromMap(importingClassMetadata.getAnnotationAttributes(ApiScan.class.getName(), true));
        assert attributes != null;
        for (Map.Entry<String, Object> entry : attributes.entrySet()) {
            if ("basePackage".equals(entry.getKey())) {
                for (String basePackage : (String[])entry.getValue()) {
                    List<String> classes = ReflectUtil.getClazzName(basePackage, true);
                    for (String aClass : classes) {
                        Class<?> clsObj = Class.forName(aClass);
                        if (!clsObj.isInterface()) {
                            continue;
                        }
                        final boolean annotationPresent = clsObj.isAnnotationPresent(ZoneMapping.class);
                        if (!annotationPresent) {
                            continue;
                        }
                        BeanDefinitionBuilder beanDefinitionBuilder = BeanDefinitionBuilder.genericBeanDefinition(ApiFactoryBean.class);
                        AbstractBeanDefinition beanDefinition = beanDefinitionBuilder.getBeanDefinition();
                        //属性注入方式
                        //构造方法注入，需要参数的构造方法
                        ConstructorArgumentValues constructorArgumentValues = beanDefinition.getConstructorArgumentValues();
                        constructorArgumentValues.addIndexedArgumentValue(0,clsObj);
                        registry.registerBeanDefinition(clsObj.getSimpleName(), beanDefinition);
                    }
                }
            }
        }
    }

}