package com.emoke.core.pangu.config;

import com.emoke.core.pangu.annotation.ZoneMapping;
import com.emoke.core.pangu.proxy.Proxy;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.context.EnvironmentAware;
import org.springframework.core.env.Environment;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.util.HashMap;
import java.util.Map;

@Slf4j
public class ApiFactoryBean<T> implements FactoryBean<T>, EnvironmentAware {
    private String urlList;
    private Map<String, String> urlMap;
    private Class<T> apiInterface;
    private Environment environment;
    public ApiFactoryBean(Class<T> apiInterface) {
        this.apiInterface = apiInterface;
    }

    @Override
    public T getObject() throws Exception {
        ZoneMapping zoneMapping = apiInterface.getAnnotation(ZoneMapping.class);
        final String url = urlMap.get(zoneMapping.url());
        editZone(zoneMapping,url);
        log.info("url:"+zoneMapping.url());
        return (T) Proxy.createProxy(apiInterface);
    }

    @Override
    public Class<?> getObjectType() {
        return apiInterface;
    }


    @Override
    public void setEnvironment(@NotNull Environment environment) {
        if(this.environment!=null){
            return;
        }
        this.environment=environment;
        urlList=environment.getProperty("pgServer");
        urlMap=parseUrl();
    }

    /**
     * 将定义好的url连接组装程map
     */
    private Map<String, String> parseUrl() {
        if (null == urlList || "".equals(urlList)) {
            throw new RuntimeException("请在yml中配置url");
        }
        Map<String, String> map = new HashMap<>();
        final String[] split = urlList.split(",");
        for (String url : split) {
            final String[] urlMap = url.split("@");
            map.put(urlMap[0], urlMap[1]);
        }
        return map;
    }

    private void editZone(ZoneMapping zoneMapping, String url) throws NoSuchFieldException, IllegalAccessException {
        //获取 ZoneMapping 这个代理实例所持有的 InvocationHandler
        InvocationHandler h = java.lang.reflect.Proxy.getInvocationHandler(zoneMapping);
        // 获取 AnnotationInvocationHandler 的 memberValues 字段
        Field hField = h.getClass().getDeclaredField("memberValues");
        // 因为这个字段事 private final 修饰，所以要打开权限
        hField.setAccessible(true);
        // 获取 memberValues
        Map<String,String> memberValues = (Map<String,String>) hField.get(h);
        // 修改 value 属性值
        memberValues.put("url", url);
    }
}
