package com.emoke.core.pangu.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.EnvironmentAware;
import org.springframework.core.env.Environment;
@Slf4j
public class AutoApiConfig implements BeanPostProcessor, EnvironmentAware {
    /**
     * 配置上下文（也可以理解为配置文件的获取工具）
     */
    private Environment evn;

    @Override
    public void setEnvironment(Environment environment) {
        this.evn=environment;
    }

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        return bean;
    }
}
