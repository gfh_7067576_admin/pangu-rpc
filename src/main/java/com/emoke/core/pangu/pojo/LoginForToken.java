package com.emoke.core.pangu.pojo;

import com.emoke.core.pangu.interfaces.HttpListener;
import com.emoke.core.pangu.resp.HttpResp;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode(callSuper = true)
@Data
@ToString
public class LoginForToken extends HttpResp implements HttpListener {
    private String access_token;
    private String token_type;
    private String expires_in;
    private String scope;

    @Override
    public boolean isSuccess() {
        return isSuccessful();
    }

}