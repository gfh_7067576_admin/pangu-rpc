package com.emoke.core.pangu.pojo;

import com.emoke.core.pangu.interfaces.HttpListener;
import com.emoke.core.pangu.resp.HttpResp;

public class Test extends HttpResp implements HttpListener {
    private String msg;

    private String code;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "Test{" +
                "msg='" + msg + '\'' +
                ", code='" + code + '\'' +
                ", isSuccess=" + isSuccess() +
                ", statusCode=" + statusCode +
                ", httpErrorMsg='" + httpErrorMsg + '\'' +
                '}';
    }

    @Override
    public boolean isSuccess() {
        return isSuccessful() && "0".equals(code);
    }
}
