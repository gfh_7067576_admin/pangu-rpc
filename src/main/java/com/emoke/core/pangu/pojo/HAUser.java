package com.emoke.core.pangu.pojo;

import com.emoke.core.pangu.interfaces.HttpListener;
import com.emoke.core.pangu.resp.HttpResp;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode(callSuper = true)
@Data
@ToString
public class HAUser extends HttpResp implements HttpListener {
    private int status;
    private String msg;
    private Object data;//true //"a"

    @Override
    public boolean isSuccess() {
        return status == 200;
    }

    @lombok.Data
    class Data {
        private String userId;
    }
}
