package com.emoke.core.pangu.pojo;

import lombok.Data;

@Data
public class LoginParam {
    private String username;
    private String password;
}
