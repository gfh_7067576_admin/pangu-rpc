package com.emoke.core.pangu.interfaces;



/**
 * @author guanfenghua
 * @date 2022/4/15 下午5:59:34
 * @description 执行接口
 */
public interface HttpExecute {
    /**
     * @description :执行http
     * @return Object
     * @exception IllegalAccessException:IllegalAccessException
     * @exception NoSuchFieldException:NoSuchFieldException
     * @exception InstantiationException:InstantiationException
     */
    Object execute() throws IllegalAccessException, NoSuchFieldException, InstantiationException;

}
