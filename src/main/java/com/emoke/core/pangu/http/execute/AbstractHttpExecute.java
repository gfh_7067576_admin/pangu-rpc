package com.emoke.core.pangu.http.execute;

import com.emoke.core.pangu.annotation.*;
import com.emoke.core.pangu.util.ReflectUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.util.TextUtils;

import java.io.File;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.HashMap;
import java.util.Map;

/**
 * @author guanfenghua
 * @date 2022/4/15 下午6:04:45
 * @description
 */
@Slf4j
public abstract class AbstractHttpExecute {
   protected String dynamicUrl;
   protected ZoneMapping zoneMapping;
   protected Method method;
   protected Object[] args;
   protected Annotation annotation;

    public AbstractHttpExecute(String dynamicUrl, ZoneMapping zoneMapping, Method method, Object[] args, Annotation annotation) {
        this.dynamicUrl = dynamicUrl;
        this.zoneMapping = zoneMapping;
        this.method = method;
        this.args = args;
        this.annotation = annotation;
    }
    /**
     * 解析json
     */
    protected Object jsonObj(Object[] args, Method method) {
        if (args != null) {
            Annotation[][] annotations = method.getParameterAnnotations();
            for (int j = 0; j < args.length; j++) {
                Object o = args[j];
                if (null == o) {
                    continue;
                }


                Annotation[] var1 = annotations[j];
                if (var1 == null || var1.length == 0) {
                    continue;
                }
                Annotation parameter = annotations[j][0];
                if (null == parameter) {
                    continue;
                }

                if (parameter instanceof Json) {
                    Parameter[] parameters = method.getParameters();
                    for (int i = 0; i < parameters.length; i++) {
                        Parameter param = parameters[i];
                        if (param.isAnnotationPresent(Json.class)) {
                            return args[i];
                        }
                    }
                }


            }
        }
        return null;
    }
    /**
     * 查询是否包含@PathVariable标签,使用rest方式url
     *
     * @param url  :请求的url
     * @param args :代理方法参数
     */
    protected String parsePathVariable(String url, Object[] args, Method method)  {
        log.info("url##############################:"+url);
        if (args != null) {
            Annotation[][] annotations = method.getParameterAnnotations();
            StringBuilder urlBuilder = new StringBuilder(url);

            for (int j = 0; j < args.length; j++) {
                Object o = args[j];
                if (null == o) {
                    continue;
                }



                Annotation[] var1 = annotations[j];
                if (var1 == null || var1.length == 0) {
                    continue;
                }
                Annotation parameter = annotations[j][0];
                if (null == parameter) {
                    continue;
                }

                if (parameter instanceof PathVariable) {
                    Parameter[] parameters = method.getParameters();
                    for (int i = 0; i < parameters.length; i++) {
                        Parameter parameter1 = parameters[i];
                        if (null != parameter1.getAnnotation(PathVariable.class)) {
                            log.info("parameter1.getName():{}--->val:{}",parameter1.getName(),args[i].toString());
                            urlBuilder = new StringBuilder(urlBuilder.toString().replace("{" + parameter1.getName() + "}", args[i].toString()));
                        }

                    }
                } else if (parameter instanceof Query) {
                    if (o instanceof Map) {
                        //拼接url
                        Map<String, Object> queryMap = (Map<String, Object>) o;
                        if (queryMap.size() == 0) {
                            continue;
                        }
                        StringBuilder splicing = new StringBuilder();
                        for (Map.Entry<String, Object> entry : queryMap.entrySet()) {
                            String mapKey = entry.getKey();
                            Object mapValue = entry.getValue();
                            if (!splicing.toString().contains("?")) {
                                splicing.append("?").append(mapKey).append("=").append(mapValue);
                                continue;
                            }

                            splicing.append("&").append(mapKey).append("=").append(mapValue);
                        }
                        urlBuilder.append(splicing.toString());
                    }
                }


            }
            url = urlBuilder.toString();
            log.info("url::::::::::::::::::::::"+url);
        }
        return url;
    }



    /**
     * 查询map组装Map<String,File>
     *
     * @param args   :参数集合
     * @param method :代理的方法
     */
    protected Map<String, File> parseMultipartMap(Object[] args, Method method) {
        if (args != null) {
            Annotation[][] annotations = method.getParameterAnnotations();
            for (int i = 0; i < args.length; i++) {
                Object o = args[i];
                if (null == o) {
                    continue;
                }
                Annotation[] var1 = annotations[i];
                if (var1 == null || var1.length == 0) {
                    continue;
                }
                Annotation parameter = annotations[i][0];
                if (null == parameter) {
                    continue;
                }

                if (parameter instanceof MultipartMap) {
                    if (o instanceof Map) {
                        return (Map<String, File>) o;
                    }
                } else if (parameter instanceof FormData) {

                }
            }
        }
        return null;
    }
    /**
     * 组装head参数
     *
     * @param args   :代理方法参数
     * @param method :代理的方法
     */
    protected Map<String, Object> head(Object[] args, Method method) {

        //定义head的map
        Map<String, Object> headMap = new HashMap<>();
        if (args != null) {
            if (!method.isAnnotationPresent(Header.class)) {
                return headMap;
            }
            Header header = method.getAnnotation(Header.class);
            String contentType = header.contentType();
            if (!TextUtils.isEmpty(contentType)) {
                headMap.put("Content-Type", contentType);
            }
            String headMapName = header.headMapName();
            if (TextUtils.isEmpty(headMapName)) {
                return headMap;
            }

            Parameter[] parameters = method.getParameters();
            for (int i = 0; i < parameters.length; i++) {
                Parameter parameter = parameters[i];
                if (!parameter.getName().equals(headMapName)) {
                    continue;
                }

                if (!(args[i] instanceof Map)) {
                    throw new RuntimeException("head must be map values");
                }

                headMap.putAll((Map<? extends String, ?>) args[i]);
            }
        }
        return headMap;
    }
    /**
     * 查询map组装
     *
     * @param args   :参数集合
     * @param method :代理方法
     */
    protected Map<String, Object> parseQuery(Object[] args, Method method) {
        if (!method.isAnnotationPresent(Header.class)) {
            throw new RuntimeException("please set head first");
        }
        Header header = method.getAnnotation(Header.class);
        String head = header.headMapName();
        if (args != null) {
            Map<String, Object> param = new HashMap<>();
            //扫描无注解参数
            Parameter[] parameters = method.getParameters();
            for (int i = 0; i < parameters.length; i++) {
                Parameter parameter = parameters[i];
                if (parameter.getAnnotations().length == 0 && null != args[i]) {
                    if (parameter.getName().equals(head)) {
                        continue;
                    }
                    if (!ReflectUtil.isPrimitive(args[i]) && parameter.getType().toString().contains("class")) {
                        //实体类
                        param.putAll(ReflectUtil.obj2Map(args[i]));
                        continue;
                    }
                    param.put(parameter.getName(), args[i]);
                }
            }
            return param;
        }
        return null;
    }

}
