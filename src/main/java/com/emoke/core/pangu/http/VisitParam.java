package com.emoke.core.pangu.http;

import lombok.Data;
import lombok.ToString;

@ToString
@Data
public class VisitParam {
    private String start;
    private String end;
    private int page;
    private int limit;
}
