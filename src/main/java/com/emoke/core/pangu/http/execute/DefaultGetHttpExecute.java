package com.emoke.core.pangu.http.execute;

import com.emoke.core.pangu.annotation.Get;
import com.emoke.core.pangu.annotation.ZoneMapping;
import com.emoke.core.pangu.http.HttpUtil;
import com.emoke.core.pangu.interfaces.HttpExecute;
import com.emoke.core.pangu.resp.HttpResp;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

/**
 * @author guanfenghua
 * @date 2022/4/15 下午6:03:47
 * @description
 */
public class DefaultGetHttpExecute extends AbstractHttpExecute implements HttpExecute {

    public DefaultGetHttpExecute(String dynamicUrl, ZoneMapping zoneMapping, Method method, Object[] args, Annotation annotation) {
        super(dynamicUrl, zoneMapping, method, args, annotation);
    }

    @Override
    public Object execute() throws IllegalAccessException, NoSuchFieldException, InstantiationException {
        return HttpUtil.getInstance().get(parsePathVariable((null==dynamicUrl?zoneMapping.url():dynamicUrl) + ((Get)annotation).path(), args, method), parseQuery(args, method), (Class<? extends HttpResp>) method.getReturnType(), method.getGenericReturnType(), head(args, method));
    }
}
