package com.emoke.core.pangu.http;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONException;
import com.emoke.core.pangu.annotation.FileStream;
import com.emoke.core.pangu.annotation.FileStreamField;
import com.emoke.core.pangu.resp.HttpResp;
import com.emoke.core.pangu.util.ReflectUtil;
import okhttp3.*;
import org.apache.http.util.TextUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.URLEncoder;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

public class HttpUtil {
    private OkHttpClient okHttpClient;
    private boolean isNeedSSL = true;
    private static final Logger logger = LoggerFactory.getLogger(HttpUtil.class);

    //    private static LoggingInterceptor loggingInterceptor =new LoggingInterceptor();
    public OkHttpClient getOkHttpClient() {
        if (null == okHttpClient) {
            synchronized (HttpUtil.class) {
                // loggingInterceptor.isLog=true;
                if (null == okHttpClient) {
                    if (isNeedSSL) {
                        okHttpClient = new OkHttpClient.Builder()
                                //设置连接超时时间
                                .connectTimeout(15*3, TimeUnit.SECONDS)
                                //设置读取超时时间
                                .readTimeout(20*3, TimeUnit.SECONDS)
                                .build();
                    } else {
                        okHttpClient = getUnsafeOkHttpClient();
                    }

                }
            }
        }
        return okHttpClient;
    }

    @SuppressWarnings("deprecation")
    private static OkHttpClient getUnsafeOkHttpClient() {
        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();
            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.sslSocketFactory(sslSocketFactory);
            builder.hostnameVerifier((hostname, session) -> true);

            return builder.connectTimeout(15*3, TimeUnit.SECONDS)
                    //设置读取超时时间
                    .readTimeout(20*3, TimeUnit.SECONDS).build();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    enum HttpUtilSingletonEnum {
        //创建一个枚举对象，该对象天生为单例
        INSTANCE;
        private final HttpUtil httpUtil;

        //私有化枚举的构造函数
        HttpUtilSingletonEnum() {
            httpUtil = new HttpUtil();
        }

        public HttpUtil getInstance() {
            return httpUtil;
        }
    }

    public static HttpUtil getInstance() {
        return HttpUtilSingletonEnum.INSTANCE.getInstance();
    }


    /**
     * get请求--无参数
     *
     * @param url      :请求地址
     * @param cls      :返回类型
     * @param paramMap :请求参数
     * @param headMap  :头部设置
     * @param type     :返回类型，包含泛型
     */
    public Object get(String url, Map<String, Object> paramMap, Class<? extends HttpResp> cls, Type type, Map<String, Object> headMap) throws NoSuchFieldException, IllegalAccessException, InstantiationException {
        Request.Builder requestBuilder = new Request.Builder();
        StringBuilder urlBuilder = new StringBuilder(url);
        if (paramMap != null) {
            urlBuilder.append("?");
            try {
                for (Map.Entry<String, Object> entry : paramMap.entrySet()) {
                    urlBuilder.append(URLEncoder.encode(entry.getKey(), "utf-8")).
                            append("=").
                            append(URLEncoder.encode(entry.getValue().toString(), "utf-8")).
                            append("&");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            urlBuilder.deleteCharAt(urlBuilder.length() - 1);
        }
        try {
            logger.info("urlBuilder.toString()=" + urlBuilder.toString());
            requestBuilder.url(urlBuilder.toString()).get();
            addHeader(requestBuilder, headMap);
            return callResult(requestBuilder, cls, type);
        } catch (Exception e) {
            e.printStackTrace();
            return exceptionResp(e, cls, url);
        }
    }


    /**
     * post请求--无file,纯参数请求
     *
     * @param url       :请求地址
     * @param cls       :返回类型
     * @param mapParams :请求参数
     * @param headMap   :头部设置
     * @param type      :返回类型，包含泛型
     */
    public Object post(String url, Map<String, Object> mapParams, Class<? extends HttpResp> cls, Type type, Map<String, Object> headMap, Object jsonObj) throws IllegalAccessException, NoSuchFieldException, InstantiationException {
        FormBody.Builder form = new FormBody.Builder();
        if (null != mapParams) {
            for (String key : mapParams.keySet()) {
                form.add(key, mapParams.get(key) + "");
                logger.info("key==" + key + ";val=" + mapParams.get(key));
            }
        }
        Request.Builder requestBuilder = buildRequest(url);
        try {
            addHeader(requestBuilder, headMap);
            if (!isJsonType(headMap)) {
                requestBuilder.post(form.build());
            } else {
                String contentType = headMap.get("Content-Type").toString();
                String json = JSON.toJSONString(jsonObj);
                logger.info("s===" + json);
                // if(true)return null;
                RequestBody requestBody = RequestBody.Companion.create(json, MediaType.Companion.parse(contentType));
                requestBuilder.post(requestBody);
            }
            return callResult(requestBuilder, cls, type);
        } catch (Exception e) {
            e.printStackTrace();
            return exceptionResp(e, cls, url);
        }
    }

    /**
     * put请求--参数请求
     *
     * @param url       :请求地址
     * @param cls       :返回类型
     * @param mapParams :请求参数
     * @param headMap   :头部设置
     * @param type      :返回类型，包含泛型
     */
    public Object put(String url, Map<String, Object> mapParams, Class<? extends HttpResp> cls, Type type, Map<String, Object> headMap, Object jsonObj) throws IllegalAccessException, NoSuchFieldException, InstantiationException {
        logger.info("url======================" + url);
        FormBody.Builder form = new FormBody.Builder();
        if (null != mapParams) {
            for (String key : mapParams.keySet()) {
                form.add(key, mapParams.get(key) + "");
            }
        }
        Request.Builder requestBuilder = buildRequest(url);
        try {
            addHeader(requestBuilder, headMap);

            if (!isJsonType(headMap)) {
                requestBuilder.put(form.build());
            } else {
                String contentType = headMap.get("Content-Type").toString();
                logger.info("jsonObj===" + JSON.toJSONString(jsonObj));
                RequestBody requestBody = RequestBody.Companion.create(JSON.toJSONString(jsonObj), MediaType.Companion.parse(contentType));
                requestBuilder.put(requestBody);
            }
            return callResult(requestBuilder, cls, type);
        } catch (Exception e) {
            e.printStackTrace();
            return exceptionResp(e, cls, url);
        }
    }


    /**
     * put请求--带有file的form-data
     *
     * @param url       :请求地址
     * @param cls       :返回类型
     * @param mapParams :请求参数
     * @param headMap   :头部设置
     * @param type      :返回类型，包含泛型
     */
    public Object put(String url, Map<String, Object> mapParams, Map<String, File> fileMap, Class<? extends HttpResp> cls, Type type, Map<String, Object> headMap, Object jsonObj) throws IllegalAccessException, NoSuchFieldException, InstantiationException {
        if (null == fileMap || fileMap.size() == 0) {
            //不存在file或者file为null或者0，此时执行无file参数post方法
            return put(url, mapParams, cls, type, headMap, jsonObj);
        }
        mapParams.remove("head");
        MultipartBody.Builder multipartBuild = new MultipartBody.Builder().setType(MultipartBody.FORM);
        for (String s : mapParams.keySet()) {
            logger.info("key = " + s + ", value = " + mapParams.get(s).toString());
            multipartBuild.addFormDataPart(s, mapParams.get(s).toString());
        }
        //遍历出file
        for (Map.Entry<String, File> entry : fileMap.entrySet()) {
            logger.info("key 00= " + entry.getKey() + ", value 00= " + entry.getValue());
            RequestBody fileBody = RequestBody.Companion.create(entry.getValue(), MediaType.Companion.parse(com.emoke.core.pangu.pojo.MediaType.FormData.getLabel()));
            multipartBuild.addFormDataPart(entry.getKey(), entry.getValue().getName(), fileBody);
        }
        Request.Builder requestBuilder = buildRequest(url);
        try {
            addHeader(requestBuilder, headMap);
            logger.info("!isJsonType(headMap)=======" + !isJsonType(headMap));
            if (!isJsonType(headMap)) {
                requestBuilder.put(multipartBuild.build());
            } else {
                logger.info("jsonObj=======" + jsonObj);
                String contentType = headMap.get("Content-Type").toString();
                RequestBody requestBody = RequestBody.Companion.create(JSON.toJSONString(jsonObj), MediaType.Companion.parse(contentType));
                requestBuilder.put(requestBody);
            }

            return callResult(requestBuilder, cls, type);
        } catch (Exception e) {
            e.printStackTrace();
            return exceptionResp(e, cls, url);
        }
    }

    /**
     * delete--参数请求
     *
     * @param url       :请求地址
     * @param cls       :返回类型
     * @param mapParams :请求参数
     * @param headMap   :头部设置
     * @param type      :返回类型，包含泛型
     */
    public Object delete(String url, Map<String, Object> mapParams, Class<? extends HttpResp> cls, Type type, Map<String, Object> headMap, Object jsonObj) throws IllegalAccessException, NoSuchFieldException, InstantiationException {
        logger.info("url=" + url);
        FormBody.Builder form = new FormBody.Builder();
        if (null != mapParams) {
            for (String key : mapParams.keySet()) {
                form.add(key, mapParams.get(key) + "");
            }
        }
        Request.Builder requestBuilder = buildRequest(url);
        try {
            addHeader(requestBuilder, headMap);
            if (!isJsonType(headMap)) {
                requestBuilder.delete(form.build());
            } else {
                String contentType = headMap.get("Content-Type").toString();
                logger.info("json====" + JSON.toJSONString(jsonObj));
                RequestBody requestBody = RequestBody.Companion.create(JSON.toJSONString(jsonObj), MediaType.Companion.parse(contentType));
                requestBuilder.delete(requestBody);
            }
            return callResult(requestBuilder, cls, type);
        } catch (Exception e) {
            e.printStackTrace();
            return exceptionResp(e, cls, url);
        }
    }

    /**
     * post请求--带有file的form-data
     *
     * @param url       :请求地址
     * @param cls       :返回类型
     * @param mapParams :请求参数
     * @param headMap   :头部设置
     * @param type      :返回类型，包含泛型
     */
    public Object post(String url, Map<String, Object> mapParams, Map<String, File> fileMap, Class<? extends HttpResp> cls, Type type, Map<String, Object> headMap, Object jsonObj) throws IllegalAccessException, NoSuchFieldException, InstantiationException {
        logger.info("url:" + url);
        if (null == fileMap || fileMap.size() == 0) {
            //不存在file或者file为null或者0，此时执行无file参数post方法
            return post(url, mapParams, cls, type, headMap, jsonObj);
        }
        MultipartBody.Builder multipartBuild = new MultipartBody.Builder().setType(MultipartBody.FORM);
        if (null != mapParams) {
            for (String s : mapParams.keySet()) {
                logger.info("key = " + s + ", value = " + mapParams.get(s).toString());
                multipartBuild.addFormDataPart(s, mapParams.get(s).toString());
            }
        }
        //遍历出file
        for (Map.Entry<String, File> entry : fileMap.entrySet()) {
            logger.info("key = " + entry.getKey() + ", value = " + entry.getValue());
            RequestBody fileBody = RequestBody.Companion.create(entry.getValue(), MediaType.Companion.parse(com.emoke.core.pangu.pojo.MediaType.FormData.getLabel()));
            multipartBuild.addFormDataPart(entry.getKey(), entry.getValue().getName(), fileBody);
        }
        Request.Builder requestBuilder = buildRequest(url);
        try {
            addHeader(requestBuilder, headMap);
            if (!isJsonType(headMap)) {
                requestBuilder.post(multipartBuild.build());
            } else {
                String contentType = headMap.get("Content-Type").toString();
                String json = JSON.toJSONString(jsonObj);
                RequestBody requestBody = RequestBody.Companion.create(json, MediaType.Companion.parse(contentType));
                requestBuilder.post(requestBody);
            }
            return callResult(requestBuilder, cls, type);
        } catch (Exception e) {
            e.printStackTrace();
            return exceptionResp(e, cls, url);
        }
    }

    /**
     * 判断content-type是否为application/json
     *
     * @param headMap :请求头map
     */
    private boolean isJsonType(Map<String, Object> headMap) {
        Object o = headMap.get("Content-Type");
        if (o == null) {
            return false;
        }

        if (TextUtils.isEmpty(o.toString())) {
            return false;
        }

        return o.toString().contains("application/json");
    }

    /**
     * 初始化Request.Builder
     *
     * @param url :请求url
     */
    private Request.Builder buildRequest(String url) {
        return new Request.Builder().url(url);
    }

    /**
     * 执行结果
     */
    private Object callResult(Request.Builder requestBuilder, Class<?> cls, Type type) throws IOException, NoSuchFieldException, IllegalAccessException, InstantiationException {
        final Request request = requestBuilder.build();
        Call call = getOkHttpClient().newCall(request);
        Response response = call.execute();
        logger.info("isSuccessful():{};code:{};msg:{}" , response.isSuccessful() , response.code() , response.message());
        if (response.isSuccessful()) {
            ResponseBody responseBody = response.body();
            if (null != responseBody) {
                if(cls.isAnnotationPresent(FileStream.class)){
                   Object o=cls.newInstance();
                    final Field[] declaredFields = o.getClass().getDeclaredFields();
                    if(declaredFields.length==0) {
                        throw new RuntimeException("no stream method");
                    }
                    for (Field field:declaredFields){
                        if(field.isAnnotationPresent(FileStreamField.class)){
                            field.setAccessible(true);
                            field.set(o,responseBody.bytes());
                            return parseResp(o,response);
                        }
                    }
                    throw new RuntimeException("no stream field");
                }
                String resp = responseBody.string();
                logger.info("resp:"+resp);
                Object o;
                try {
                    o = JSON.parseObject(resp, type);
                } catch (JSONException e) {
                    //返回的数据这块object不明确，
                    o = JSON.parseObject(resp, cls);
                }
                return parseResp(o, response);
            } else {
                logger.info("无任何返回数据=--");
            }
        } else {
            return parseResp(ReflectUtil.newInstance(cls), response);
        }
        return null;
    }

    /**
     * 处理异常
     *
     * @param e           :异常父类
     * @param cls:目标class
     * @param url         :请求url
     */
    private Object exceptionResp(Exception e, Class<?> cls, String url) throws InstantiationException, IllegalAccessException, NoSuchFieldException {
        if (e instanceof NoSuchFieldException || e instanceof IllegalAccessException || e instanceof InstantiationException) {
            Object o = ReflectUtil.newInstance(cls);
            Class<?> superClass = o.getClass().getSuperclass();
            superClass.getDeclaredField(HttpResp.IS_SUCCESS).set(o, false);
            superClass.getDeclaredField(HttpResp.STATUS_CODE).set(o, -1);
            superClass.getDeclaredField(HttpResp.HTTP_ERROR_MSG).set(o, "未知错误!");
        } else if (e instanceof SocketTimeoutException) {
            Object o = ReflectUtil.newInstance(cls);
            Class<?> superClass = o.getClass().getSuperclass();
            superClass.getDeclaredField(HttpResp.IS_SUCCESS).set(o, false);
            superClass.getDeclaredField(HttpResp.STATUS_CODE).set(o, 408);
            superClass.getDeclaredField(HttpResp.HTTP_ERROR_MSG).set(o, "请求超时!");
            return o;
        } else if (e instanceof ConnectException) {
            Object o = ReflectUtil.newInstance(cls);
            Class<?> superClass = o.getClass().getSuperclass();
            superClass.getDeclaredField(HttpResp.IS_SUCCESS).set(o, false);
            superClass.getDeclaredField(HttpResp.STATUS_CODE).set(o, 408);
            superClass.getDeclaredField(HttpResp.HTTP_ERROR_MSG).set(o, "无法连接" + url);
            return o;
        } else {
            Object o = ReflectUtil.newInstance(cls);
            Class<?> superClass = o.getClass().getSuperclass();
            superClass.getDeclaredField(HttpResp.IS_SUCCESS).set(o, false);
            superClass.getDeclaredField(HttpResp.STATUS_CODE).set(o, 500);
            superClass.getDeclaredField(HttpResp.HTTP_ERROR_MSG).set(o, "未知错误:" + e.toString());
            return o;
        }
        return null;
    }

    private void addHeader(Request.Builder requestBuilder, Map<String, Object> headMap) {
        if (null == headMap) {
            return;
        }
        logger.info("headMap===========" + headMap.toString());
        for (Map.Entry<String, Object> entry : headMap.entrySet()) {
            if (null != entry.getValue()) {
                requestBuilder.addHeader(entry.getKey(), entry.getValue().toString());
            }
        }
        requestBuilder.build();
    }

    /**
     * 组装基础object
     *
     * @param o        :原始子类
     * @param response :http返回响应
     */
    private Object parseResp(Object o, Response response) throws NoSuchFieldException, IllegalAccessException, IOException {
        Class<?> superClass = o.getClass().getSuperclass();
        superClass.getDeclaredField(HttpResp.IS_SUCCESS).set(o, response.isSuccessful());
        superClass.getDeclaredField(HttpResp.STATUS_CODE).set(o, response.code());
        superClass.getDeclaredField(HttpResp.HEADERS).set(o, response.headers());
        if (response.isSuccessful()) {
            superClass.getDeclaredField(HttpResp.HTTP_ERROR_MSG).set(o, response.message());
        } else {
            superClass.getDeclaredField(HttpResp.HTTP_ERROR_MSG).set(o, Objects.requireNonNull(response.body()).string());
        }

        superClass.getDeclaredField(HttpResp.HEADERS).set(o, response.headers());
        return o;
    }

    /**
     * 流返回
     * */
    private Object parseStreamResp(Object o, Response response) throws NoSuchFieldException, IllegalAccessException, IOException {
        Class<?> superClass = o.getClass().getSuperclass();
        superClass.getDeclaredField(HttpResp.IS_SUCCESS).set(o, response.isSuccessful());
        superClass.getDeclaredField(HttpResp.STATUS_CODE).set(o, response.code());
        superClass.getDeclaredField(HttpResp.HEADERS).set(o, response.headers());
        if (response.isSuccessful()) {
            superClass.getDeclaredField(HttpResp.HTTP_ERROR_MSG).set(o, response.message());
        } else {
            superClass.getDeclaredField(HttpResp.HTTP_ERROR_MSG).set(o, Objects.requireNonNull(response.body()).string());
        }

        superClass.getDeclaredField(HttpResp.HEADERS).set(o, response.headers());
        return o;
    }



}