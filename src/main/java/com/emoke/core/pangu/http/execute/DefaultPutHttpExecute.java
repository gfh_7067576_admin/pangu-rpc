package com.emoke.core.pangu.http.execute;

import com.emoke.core.pangu.annotation.Put;
import com.emoke.core.pangu.annotation.ZoneMapping;
import com.emoke.core.pangu.http.HttpUtil;
import com.emoke.core.pangu.interfaces.HttpExecute;
import com.emoke.core.pangu.resp.HttpResp;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

/**
 * @author guanfenghua
 * @date 2022/4/16 上午9:20:24
 * @description
 */
public class DefaultPutHttpExecute extends AbstractHttpExecute implements HttpExecute {

    public DefaultPutHttpExecute(String dynamicUrl, ZoneMapping zoneMapping, Method method, Object[] args, Annotation annotation) {
        super(dynamicUrl, zoneMapping, method, args, annotation);
    }

    @Override
    public Object execute() throws IllegalAccessException, NoSuchFieldException, InstantiationException {
        return HttpUtil.getInstance().put(parsePathVariable((null==dynamicUrl?zoneMapping.url():dynamicUrl) + ((Put)annotation).path(), args, method), parseQuery(args, method), parseMultipartMap(args, method), (Class<? extends HttpResp>) method.getReturnType(), method.getGenericReturnType(), head(args, method), jsonObj(args, method));
    }
}