package com.emoke.core.pangu.annotation;

import java.lang.annotation.*;

/**
 * @author guanfenghua
 */
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface PathVariable {

}