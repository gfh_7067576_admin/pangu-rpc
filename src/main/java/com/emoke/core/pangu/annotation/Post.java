package com.emoke.core.pangu.annotation;

import java.lang.annotation.*;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Post {
    /**
     * 请求地址
     */
    String path();

}