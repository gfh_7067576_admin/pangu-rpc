package com.emoke.core.pangu.api;

import com.emoke.core.pangu.annotation.*;
import com.emoke.core.pangu.pojo.HAUser;

import java.util.Map;

@ZoneMapping(url = "test")
public interface HAApi {
    /**
     * 接口: 1.1 获取企业用户详情
     */
    @Header(contentType = "application/json")
    @Get(path = "/AGCS_zqc/v2/audit/me/{openid}")
    HAUser get(@Query Map<String, Object> map, @PathVariable String openid);

}