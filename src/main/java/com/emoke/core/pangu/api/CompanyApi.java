package com.emoke.core.pangu.api;

import com.emoke.core.pangu.annotation.Header;
import com.emoke.core.pangu.annotation.Post;
import com.emoke.core.pangu.annotation.Query;
import com.emoke.core.pangu.annotation.ZoneMapping;
import com.emoke.core.pangu.pojo.LoginForToken;

import java.util.Map;

@ZoneMapping(url = "test")
public interface CompanyApi {
    /**
     * 1.1 企业端-根据clientid和clientSecret获取token
     */
    @Header(contentType = "application/x-www-form-urlencoded")
    @Post(path = "/oauth/token")
    LoginForToken loginForToken(@Query Map<String, Object> map);

}