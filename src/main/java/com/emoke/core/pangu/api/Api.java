package com.emoke.core.pangu.api;

import com.emoke.core.pangu.annotation.*;
import com.emoke.core.pangu.pojo.LoginParam;
import com.emoke.core.pangu.pojo.Test;

import java.io.File;
import java.util.Map;

@ZoneMapping(url = "test")
public interface Api {

    @Get(path = "/pass/check/{id}")
    Test get(@Query Map<String, Object> map);

    @Post(path = "/pass/check")
    Test post(@MultipartMap Map<String, File> fileMap);

    @Header(contentType = "form-data", headMapName = "head")
    @Put(path = "/pass/put/{id}")
    Test put(@Query Map<String, Object> map, @PathVariable String id, Map<String, Object> head);

    @Header(contentType = "form-data", headMapName = "head")
    @Delete(path = "/pass/put/{id}")
    Test delete(@Query Map<String, Object> map, @PathVariable String id, Map<String, Object> head);

    @Post(path = "/admin/login")
    Test admin(String username, String password);

    @Post(path = "/admin/login")
    Test adminLogin(LoginParam loginParam);


}