package com.emoke.core.pangu;//package com.emoke.core.pangu;
//
//import com.emoke.core.pangu.annotation.ZoneMapping;
//import com.emoke.core.pangu.proxy.Proxy;
//import com.emoke.core.pangu.util.ReflectUtil;
//
//import java.lang.reflect.Field;
//import java.lang.reflect.InvocationHandler;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//import java.util.concurrent.ConcurrentHashMap;
//
///**
// * 网络辅助核心类
// */
//public class NetAuxiliary {
//    private final ConcurrentHashMap<String, Object> proxyObjMap = new ConcurrentHashMap<>();
//    private String urlList;
//    private Map<String, String> urlMap;
//
//    public NetAuxiliary() {
//
//    }
//
//    public void setBasePackage(String basePackage) throws ClassNotFoundException, NoSuchFieldException, IllegalAccessException {
//        List<String> classes = ReflectUtil.getClazzName(basePackage, true);
//        for (String aClass : classes) {
//            Class<?> clsObj = Class.forName(aClass);
//            if (!clsObj.isInterface()) continue;
//            final boolean annotationPresent = clsObj.isAnnotationPresent(ZoneMapping.class);
//            if (!annotationPresent) continue;
//            ZoneMapping zoneMapping = clsObj.getAnnotation(ZoneMapping.class);
//            final String url = urlMap.get(zoneMapping.url());
//          //  editZone(zoneMapping,url);
//            Object obj = Proxy.createProxy(clsObj);
//            proxyObjMap.put(aClass, obj);
//
//        }
//    }
//
//    private void editZone(ZoneMapping zoneMapping, String url) throws NoSuchFieldException, IllegalAccessException {
//        //获取 ZoneMapping 这个代理实例所持有的 InvocationHandler
//        InvocationHandler h = java.lang.reflect.Proxy.getInvocationHandler(zoneMapping);
//        // 获取 AnnotationInvocationHandler 的 memberValues 字段
//        Field hField = h.getClass().getDeclaredField("memberValues");
//        // 因为这个字段事 private final 修饰，所以要打开权限
//        hField.setAccessible(true);
//        // 获取 memberValues
//        Map<String,String> memberValues = (Map<String,String>) hField.get(h);
//        // 修改 value 属性值
//        memberValues.put("url", url);
//    }
//
//    public void setBasePackage(String[] basePackages, String urlList) throws ClassNotFoundException, NoSuchFieldException, IllegalAccessException {
//        this.urlList = urlList;
//        urlMap = parseUrl();
//        for (String basePackage : basePackages) setBasePackage(basePackage);
//    }
//
//    public <T> T getApi(Class<T> cls) {
//        return (T) proxyObjMap.get(cls.getName());
//    }
//
//    /**
//     * 将定义好的url连接组装程map
//     */
//    private Map<String, String> parseUrl() {
//        if (null == urlList || "".equals(urlList)) throw new RuntimeException("请在yml中配置url");
//        Map<String, String> map = new HashMap<>();
//        final String[] split = urlList.split(",");
//        for (String url : split) {
//            final String[] urlMap = url.split("@");
//            map.put(urlMap[0], urlMap[1]);
//        }
//        return map;
//    }
//
//    public static void main(String[] args) throws ClassNotFoundException, IllegalAccessException, NoSuchFieldException {
//        NetAuxiliary netAuxiliary = new NetAuxiliary();
//        netAuxiliary.setBasePackage(new String[]{"com.emoke.core.pangu.api","com.emoke.core.sso.api"},"test@http://www.baidu.com");
//
//    }
//}