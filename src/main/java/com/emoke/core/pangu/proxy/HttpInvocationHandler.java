package com.emoke.core.pangu.proxy;

import com.emoke.core.pangu.annotation.*;
import com.emoke.core.pangu.http.execute.DefaultDeleteHttpExecute;
import com.emoke.core.pangu.http.execute.DefaultGetHttpExecute;
import com.emoke.core.pangu.http.execute.DefaultPostHttpExecute;
import com.emoke.core.pangu.http.execute.DefaultPutHttpExecute;
import com.emoke.core.pangu.interfaces.HttpExecute;
import lombok.extern.slf4j.Slf4j;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * @author guanfenghua
 */
@Slf4j
public class HttpInvocationHandler implements InvocationHandler {

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        //已实现的具体类
        if (Object.class.equals(method.getDeclaringClass())) {
            try {
                return method.invoke(this, args);
            } catch (Throwable t) {
                t.printStackTrace();
            }
            //接口
        } else {
            Get get = method.getAnnotation(Get.class);
            Post post = method.getAnnotation(Post.class);
            Put put = method.getAnnotation(Put.class);
            Delete delete = method.getAnnotation(Delete.class);
            String url = getUrl(get, post, put, delete);
            long t1 = System.currentTimeMillis();
            Object o = run(method, args, get, post, put, delete);
            log.info("api:" + url + " --> response time:" + (System.currentTimeMillis() - t1) + "ms");
            return o;
        }
        return null;
    }

    /**
     * 获取url，计算时间要用到
     *
     * @param get    :get
     * @param post   :post
     * @param put    :put
     * @param delete :delete
     */
    private String getUrl(Get get, Post post, Put put, Delete delete) {
        String url;
        if (null != get) {
            url = get.path();
        } else if (null != post) {
            url = post.path();
        } else if (null != put) {
            url = put.path();
        } else if (null != delete) {
            url = delete.path();
        } else {
            throw new RuntimeException("Request method comment not found");
        }

        return url;
    }

    /**
     * 执行http请求
     *
     * @param args   :代理方法参数
     * @param get    :get
     * @param post   :post
     * @param put    :put
     * @param delete :delete
     * @param method :代理的方法
     * @return :Object
     * @throws IllegalAccessException, NoSuchFieldException, InstantiationException
     */
    public Object run(Method method, Object[] args, Get get, Post post, Put put, Delete delete) throws IllegalAccessException, NoSuchFieldException, InstantiationException {
        ZoneMapping zoneMapping = method.getDeclaringClass().getAnnotation(ZoneMapping.class);
        if (null == zoneMapping) {
            throw new RuntimeException("Domain name not found,please set ZoneMapping annotation");
        }

        //解析动态url
        String dynamicUrl = dynamicUrl(args, method);
        HttpExecute httpExecute;
        if (null != get) {
            httpExecute = new DefaultGetHttpExecute(dynamicUrl, zoneMapping, method, args, get);
        } else if (null != post) {
            httpExecute = new DefaultPostHttpExecute(dynamicUrl, zoneMapping, method, args, post);
        } else if (null != put) {
            httpExecute = new DefaultPutHttpExecute(dynamicUrl, zoneMapping, method, args, put);
        } else if (null != delete) {
            httpExecute = new DefaultDeleteHttpExecute(dynamicUrl, zoneMapping, method, args, delete);
        } else {
            throw new RuntimeException("Request method comment not found");
        }
        return httpExecute.execute();
    }


    /**
     * 解析动态url
     */
    private String dynamicUrl(Object[] args, Method method) {
        if (args != null) {
            Annotation[][] annotations = method.getParameterAnnotations();
            for (int i = 0; i < args.length; i++) {
                Object o = args[i];
                if (null == o) {
                    continue;
                }
                Annotation[] var1 = annotations[i];
                if (var1 == null || var1.length == 0) {
                    continue;
                }
                Annotation parameter = annotations[i][0];
                if (null == parameter) {
                    continue;
                }
                if (parameter instanceof DynamicUrl) {
                    return (String) o;
                }
            }
        }
        return null;
    }


}
